# Prevent brp-python-bytecompile from running.
%define __os_install_post %{___build_post}

# "Harbour RPM packages should not provide anything."
%define __provides_exclude_from ^%{_datadir}/.*$

Name: harbour-hasher
Version: 0.5.0
Release: jolla
Summary: Application to handle hash digests
License: GPLv3+
URL: https://gitlab.com/nobodyinperson/harbour-hasher
Source: %{name}-%{version}.tar.xz
Packager: Yann Büchau <nobodyinperson@posteo.de>
Conflicts: %{name}
BuildArch: noarch
BuildRequires: gettext
BuildRequires: inkscape
BuildRequires: gettext
BuildRequires: pandoc
BuildRequires: make
Requires: pyotherside-qml-plugin-python3-qt5 >= 1.4
Requires: libsailfishapp-launcher
Requires: python3-base
Requires: python3-base91 >= 1.0
Requires: python3-xdgspec >= 0.2
Requires: sailfishsilica-qt5

%description
Handle hash digests

%prep
%setup -q

%install
./configure --prefix=/usr
make
make DESTDIR=%{buildroot} install

%files
%defattr(-,root,root,-)
%{_datadir}/icons/hicolor/*/apps/%{name}.png
%{_datadir}/icons/hicolor/*/apps/%{name}.svg
%{_datadir}/applications/%{name}.desktop
%{_datadir}/%{name}/qml/*.qml
%{_datadir}/%{name}/python/*.py
%{_datadir}/%{name}/images/*.svg
%{_datadir}/%{name}/translations/*.qm
%{_datadir}/%{name}/locale/*/LC_MESSAGES/%{name}.mo

%changelog
* Sun Oct 27 2019 Yann Büchau <nobodyinperson@posteo.de> 0.5.0-jolla
- iterative hashing (with or without encoding)
- adaptive font sizes
- improved error handling
* Mon Oct 26 2019 Yann Büchau <nobodyinperson@posteo.de> 0.4.0-jolla
- Add Swedish translation from Åke Engelbrektson
- Add French translation
- Show translators in About page
* Mon Oct 25 2019 Yann Büchau <nobodyinperson@posteo.de> 0.3.2-jolla
- fix notification bar preventing upper taps
* Mon Oct 25 2019 Yann Büchau <nobodyinperson@posteo.de> 0.3.1-jolla
- fix Python ImportError
* Mon Oct 25 2019 Yann Büchau <nobodyinperson@posteo.de> 0.3.0-jolla
- This release adds the following features:
- an About Page
- selecting the input encoding
- examining the input in different encodings
- selecting the hash digest encoding
- separating displayed encoded bytes with whitespace
- overall smoother UI
- notification bar for infos and warnings
* Mon Oct 21 2019 Yann Büchau <nobodyinperson@posteo.de> 0.2.0-jolla
- Fix algorithm list not being filled sometimes
- Calculate hash dynamically when input changes
- Add PullDownMenu to clear the input
- Add button to use digest as new input for iterative hashing
* Sun Oct 20 2019 Yann Büchau <nobodyinperson@posteo.de> 0.1.0-jolla
- basic text hashing functionality
- selectable hashing algorithm
- copying digest to clipboard
