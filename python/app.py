# pyotherside
import pyotherside

# system modules
import logging
import hashlib

# internal modules
from . import l10n
from . import config as cfg
from . import encodings
from .configuration import Configuration

# external modules


logger = logging.getLogger(__name__)


def transform(*args, **kwargs):
    try:
        return encodings.transform(*args, **kwargs)
    except encodings.EncodingError as e:
        pyotherside.send("error", str(e))
        return None


def available_algorithm():
    """
    Generator yielding available algorithms without an extra argument
    """
    for algorithm in hashlib.algorithms_guaranteed:
        hashfunc = hashlib.new(algorithm)
        try:
            hashfunc.hexdigest()
            yield algorithm
        except TypeError:
            pass


def available_algorithms():
    return sorted(tuple(available_algorithm()))


def hash(
    s,
    from_encoding,
    algorithm,
    to_encoding,
    iterations=0,
    encoded_iteration=False,
    separate_bytes=False,
):
    try:
        decoded = encodings.decode(s, from_encoding)
        hash_input = decoded
        iterations = int(max(0, iterations))
        for i in range(iterations + 1):
            hashfunc = hashlib.new(algorithm)
            if encoded_iteration and i:
                hash_input = encodings.encode(
                    hash_input, to_encoding, separate_bytes
                ).encode()
            hashfunc.update(hash_input)
            digest = hashfunc.digest()
            hash_input = digest
        encoded = encodings.encode(digest, to_encoding, separate_bytes)
        logger.debug(
            "{to_encoding}-encoded {algo} hash with "
            "{iterations} iterations of {text} "
            "decoded with {from_encoding} is {encoded}".format(
                iterations=iterations,
                from_encoding=from_encoding,
                to_encoding=to_encoding,
                algo=algorithm,
                text=repr(s),
                encoded=repr(encoded),
            )
        )
        return encoded
    except encodings.EncodingError as e:
        pyotherside.send("error", str(e))
        return None


logger.debug("Sending encoders {} to QML".format(encodings.encoders))
pyotherside.send("encoders", encodings.encoders)

logger.debug("Sending input decoders {} to QML".format(encodings.decoders))
pyotherside.send("decoders", encodings.decoders)

logger.debug("Sending algorithms {} to QML".format(available_algorithms()))
pyotherside.send("hash-algorithms", available_algorithms())

logger.debug("Sending app version {} to QML".format(cfg.VERSION))
pyotherside.send("app-version", cfg.VERSION)

logger.debug("Reading user configuration")
config = Configuration()
config.read_home()

logger.debug("Python module loaded")
