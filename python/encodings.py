# system modules
import logging
import itertools
import operator
import base64
import re

# internal modules

# external modules
try:
    import base91
except ImportError:

    class Fake:
        def __bool__(self):
            return False

    base91 = Fake()

logger = logging.getLogger(__name__)

decoders = []
encoders = []


def decoder(name=None, translated=None, **kwargs):
    """
    Create a generator to register a decoder under a given name
    """

    def decorator(fun):
        decoders.append(
            {
                "name": {
                    "raw": fun.__name__ if name is None else name,
                    "translated": fun.__name__
                    if translated is None
                    else translated,
                },
                "str2bytes": fun,
                **kwargs,
            }
        )
        return fun

    return decorator


def encoder(name=None, translated=None, **kwargs):
    """
    Create a generator to register an encoder under a given name
    """

    def decorator(fun):
        encoders.append(
            {
                "name": {
                    "raw": fun.__name__ if name is None else name,
                    "translated": fun.__name__
                    if translated is None
                    else translated,
                },
                "bytes2str": fun,
                **kwargs,
            }
        )
        return fun

    return decorator


basenames = dict(
    zip(
        (2, 8, 10, 16),
        (_("binary"), _("octal"), _("decimal"), _("hexadecimal")),
    )
)


class BasedIntError(ValueError):
    pass


def basedint2byte(base):
    def converter(s):
        try:
            return bytes((int(s, base),))
        except BaseException as e:
            raise ValueError(
                _("Cannot interpret {string} as {type}: {error}").format(
                    string=repr(s), error=e, type=basenames[base]
                )
            )

    return converter


@decoder("utf8", _("UTF-8"), bytewise=False)
def utf8str2bytes(s, sep=False):
    # if sep:
    #     pyotherside.send(
    #         "warning",
    #         _("Separating unicode with space doesn't make much sense"),
    #     )
    return s.encode()


# @encoder("utf8", _("UTF-8"), bytewise=False)
# def bytes2utf8str(b, sep=False):
#     if sep:
#         logger.warning(
#             "Separating unicode with space " "doesn't make much sense"
#         )
#     return b.decode("utf8", errors="replace")


@decoder("base64", _("base64"), bytewise=False)
def base64str2bytes(s, sep=False):
    # if sep:
    #     pyotherside.send(
    #         "warning", _("Reading separated Base64 is not yet implemented!")
    #     )
    return base64.decodebytes(s.encode("utf8"))


@encoder("base64", _("base64"), bytewise=False)
def bytes2base64str(b, sep=False):
    # if sep:
    #     pyotherside.send(
    #         "warning", _("Encoding separated Base64 is not yet implemented!")
    #     )
    return re.sub(r"\s+", "", base64.encodebytes(b).decode())


class Base91Error(ValueError):
    pass


if hasattr(base91, "encode"):

    @encoder("base91", _("BasE91"), bytewise=False)
    def bytes2base91str(b, sep=False):
        # if sep:
        #     pyotherside.send(
        #         "warning",
        #         _("Encoding separated Base91 is not yet implemented!"),
        #     )
        return base91.encode(b)


if hasattr(base91, "decode"):

    @decoder("base91", _("BasE91"), bytewise=False)
    def bytes2base91str(s, sep=False):
        # if sep:
        #     pyotherside.send(
        #         "warning",
        #         _("Decoding separated Base91 is not yet implemented!"),
        #     )
        invalid = set(s) - set(base91.base91_alphabet)
        if invalid:
            raise Base91Error(
                _(
                    "invalid BasE91 characters: {characters}".format(
                        characters="".join(invalid)
                    )
                )
            )
        return base91.decode(s)


@decoder("binary", _("binary"), bytewise=True)
def binarystr2bytes(s, sep=False):
    return bytes(
        itertools.chain.from_iterable(
            map(
                basedint2byte(2),
                map(
                    operator.methodcaller("group", 1),
                    re.finditer(r"(\S{1,8})\s*", s),
                ),
            )
        )
    )


@encoder("binary", _("binary"), bytewise=True)
def bytes2binarystr(b, sep=False):
    return (" " if sep else "").join(map("{:08b}".format, b))


@decoder("octal", _("octal"), bytewise=True)
def binarystr2bytes(s, sep=False):
    return bytes(
        itertools.chain.from_iterable(
            map(
                basedint2byte(8),
                map(
                    operator.methodcaller("group", 1),
                    re.finditer(r"(\S{1,3})\s*", s),
                ),
            )
        )
    )


@encoder("octal", _("octal"), bytewise=True)
def bytes2hexstr(b, sep=False):
    return (" " if sep else "").join(map("{:03o}".format, b))


@decoder("decimal", _("decimal"), bytewise=True)
def binarystr2bytes(s, sep=False):
    return bytes(
        itertools.chain.from_iterable(
            map(
                basedint2byte(10),
                map(
                    operator.methodcaller("group", 1),
                    re.finditer(r"(\S{1,3})\s*", s),
                ),
            )
        )
    )


@encoder("decimal", _("decimal"), bytewise=True)
def bytes2decimalstr(b, sep=False):
    return (" " if sep else "").join(map("{:03d}".format, b))


@decoder("hexadecimal", _("hexadecimal"), bytewise=True)
def binarystr2bytes(s, sep=False):
    return bytes(
        itertools.chain.from_iterable(
            map(
                basedint2byte(16),
                map(
                    operator.methodcaller("group", 1),
                    re.finditer(r"(\S{1,2})\s*", s),
                ),
            )
        )
    )


@encoder("hex", _("hexadecimal"), bytewise=True)
def bytes2hexstr(b, sep=False):
    return (" " if sep else "").join(map("{:02x}".format, b))


class EncodingError(ValueError):
    pass


def encode(b, enc, sep=False):
    fun = next(
        filter(lambda x: x.get("name", {}).get("raw") == enc, encoders),
        {"bytes2str": lambda x: x},
    ).get("bytes2str", lambda x: x)
    try:
        return fun(b, sep)
    except BaseException as e:
        raise EncodingError(
            _("{encoding} encoding error: {error}").format(
                encoding=enc, error=e
            )
        )


def decode(s, enc, sep=False):
    fun = next(
        filter(lambda x: x.get("name", {}).get("raw") == enc, decoders),
        {"str2bytes": lambda x: x},
    ).get("str2bytes", lambda x: x)
    try:
        return fun(s, sep)
    except BaseException as e:
        raise EncodingError(
            _("{encoding} decoding error: {error}").format(
                encoding=enc, error=e
            )
        )


def transform(s, from_encoding, to_encoding, separate_bytes=False):
    decoded = decode(s, from_encoding)
    encoded = encode(decoded, to_encoding, separate_bytes)
    logger.debug(
        "{text} transformed from {from_encoding} "
        "to {to_encoding} is {result}".format(
            text=repr(s),
            from_encoding=from_encoding,
            to_encoding=to_encoding,
            result=repr(encoded),
        )
    )
    return encoded
