# system modules
import os
import gettext
import locale

# internal modules
from . import config

# external modules

GETTEXT_DOMAIN = config.PACKAGE_NAME
LOCALEDIR = os.path.join(config.datadir, config.PACKAGE_NAME, "locale")
locale.setlocale(locale.LC_ALL, "")
for mod in (locale, gettext):
    mod.bindtextdomain(GETTEXT_DOMAIN, LOCALEDIR)
gettext.textdomain(GETTEXT_DOMAIN)
gettext.install(GETTEXT_DOMAIN, localedir=LOCALEDIR)
