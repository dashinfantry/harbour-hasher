# pyotherside
import pyotherside

# system modules
import os
import re
import configparser
import itertools
import logging

# internal modules
from . import config as cfg

# external modules
import xdgspec

logger = logging.getLogger(__name__)

logger.info(
    "User config directory: {}".format(
        xdgspec.XDGPackageDirectory("XDG_CONFIG_HOME", cfg.PACKAGE_NAME).path
    )
)


class Configuration(configparser.ConfigParser):
    UI_SECTION = "ui"

    @property
    def home_config_xdgdir(self):
        return xdgspec.XDGPackageDirectory("XDG_CONFIG_HOME", cfg.PACKAGE_NAME)

    @property
    def home_config_file(self):
        with self.home_config_xdgdir as pkgdir:
            return os.path.join(pkgdir, "{}.conf".format(cfg.PACKAGE_NAME))

    def write_home(self):
        with open(self.home_config_file, "w") as f:
            self.write(f)

    def read_home(self):
        self.read(self.home_config_file)
