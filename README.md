# Hasher

**Hasher** is a [SailfishOS](https://sailfishos.org) application to
manage hash digests.

## Installation

You may install **Hasher** from
[OpenRepos](https://openrepos.net/content/nobodyinperson/hasher) on your
SailfishOS device.

## Development

### Requirements

To build the app, you only need a couple of standard tools. **You don't need
the Sailfish SDK!**. On a Debian-like system, you should be fine installing
these packages (output of snippet $1754230):

```bash
sudo apt update && sudo apt install gawk xz-utils coreutils perl-base rpm gettext inkscape pandoc make findutils openssh-client
```

To set up the build environment, `git clone` the repository and then run once
from the repository root:

```bash
git submodule update --init --recursive
autoreconf --install
./configure --prefix=/usr
```

This produces all the necessary Makefiles and the `control` Makefile.

### Building the RPM package

```bash
./control rpm
```

## Testing the package on the device

This is implemented via SSH/SCP. Set the parameter `device_ssh_host` to the
hostname of your Sailfish device that you have network access to. This also
works for the SailfishOS Emulator. It is recommended that you specify the SSH
settings in `.ssh/config`.

In all of the following commands, you can always hand the `-n` parameter to
check what the command would do without executing anything.

### Deploying the RPM package to a device

To (build and) deploy the rpm to the device via `scp`, run

```bash
./control deploy device_ssh_host=jolla
```

### Installing the RPM on the device

To (build, deploy and) install the RPM on the device, run

```bash
./control install-device device_ssh_host=jolla
```

If you get an error like `Failed to obtain authentication`, you can use
`install_precmd=devel-su` (or `install_precmd=sudo` if you have `sudo`
configured) to run the installation with root privileges:

```bash
./control install-device device_ssh_host=jolla install_precmd=sudo
```

### Running the app on the device

To (build, deploy, install and) run the application on the device, run

```bash
./control run device_ssh_host=jolla
```

It might be necessary to source your shell configuration. For BASH, specify
`run_precmd=". ~/.bashrc &&"`
and for ZSH, specify `run_precmd=". ~/.zshrc &&"`:

```bash
./control run device_ssh_host=jolla run_precmd=". ~/.zshrc &&"
```

If you want to run the application with a different language, e.g. French,
append `LANG=fr_FR.utf8` to the `run_precmd=` parameter.

On a Jolla 1 (configured in `.ssh/config` as `jolla`) with `zsh` as shell, the
oneliner to test the app should read:

```bash
./control run device_ssh_host=jolla install_precmd=devel-su run_precmd=". ~/.zshrc &&"
```

If you don't want to type your root password on every installation, put it in a
file `sfos-passwd.sh`:

```bash
export SFOS_PASSWD=your-secret-password
```

Then, to deploy and run, execute

```bash
source sfos-passwd.sh && ./control run device_ssh_host=jolla install_precmd="echo $SFOS_PASSWD | devel-su" run_precmd=". ~/.zshrc &&"
```

With this one-liner, you build the application, deploy it onto your SailfishOS device **and** open the app there. Nice, huh? :-)
