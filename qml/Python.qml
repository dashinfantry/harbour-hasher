import QtQuick 2.0
import io.thp.pyotherside 1.4

Python {
    property var hashAlgorithms: new Array()
    property var encoders: new Array()
    property var decoders: new Array()

    Component.onCompleted: {
        addImportPath(Qt.resolvedUrl('..'));
        importModule('python', function () {
            console.debug("Python module imported")
        });
        setHandler("hash-algorithms", function(algorithms) {
            console.debug(
                "Recieved hash algorithm list from python: %1".arg(algorithms))
            hashAlgorithms = algorithms
        })
        setHandler("decoders", function(d) {
            console.debug("Recieved decoders list from python: %1".arg(d))
            decoders = d
        })
        setHandler("encoders", function(e) {
            console.debug("Recieved encoders list from python: %1".arg(e))
            encoders = e
        })
     }

    onError: {
        // when an exception is raised, this error handler will be called
        console.log('python error: ' + traceback);
    }

    onReceived: {
        // asychronous messages from Python arrive here
        // in Python, this can be accomplished via pyotherside.send()
        console.debug('got message from python: ' + data);
    }
}

