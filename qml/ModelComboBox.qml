import QtQuick 2.0
import Sailfish.Silica 1.0

ComboBox {
    property var selectionModel: ListModel {}
    property var elementToDisplayText: function(e){return JSON.stringify(e)}
    property var elementToRawText:     function(e){return JSON.stringify(e)}
    property var rawValue: elementToRawText(selectionModel.get(currentIndex))
    menu: ContextMenu {
        Repeater {
            model: selectionModel
            delegate: MenuItem {
                text: elementToDisplayText(model)
            }
        }
    }
    Component.onCompleted: {
        // - currentIndex defaults to 0
        // - when the model is filled, the value is changed, but the
        //   currentIndex stays at 0 (because that's what is is, really)
        // - to also emit the signal that currentIndex changed on the first
        //   model fill, we need this workaround
        selectionModel.countChanged.connect(function(){
            var oldCurrentIndex = currentIndex
            currentIndex = -1
            currentIndex = oldCurrentIndex
        })
    }
}
