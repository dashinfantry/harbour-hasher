import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: firstpage
    property var examineInput: false

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        PullDownMenu {
            MenuItem {
                text: qsTranslate("first-page","About Hasher")
                onClicked: app.pageStack.push("AboutPage.qml")
            }
            MenuItem {
                enabled: inputField.text != ""
                text: qsTranslate("first-page","Clear")
                onClicked: {
                    inputField.text = ""
                    digestField.text = ""
                    inputBytesField.text = ""
                    }
            }
        }

        Column {
            id: column
            spacing: Theme.paddingMedium
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: qsTranslate("first-page","Hasher")
            }

            SectionHeader {
                text: qsTranslate("first-page", "Input")
            }

            ListModel {
                id: decodersModel
                Component.onCompleted: {
                    python.decodersChanged.connect(function(){
                        console.debug("Updating input encoding " +
                            "selections with %1".arg(python.decoders))
                        clear()
                        python.decoders.forEach(function(decoder){
                            append(decoder)
                        })
                    })
                }
            }

            ModelComboBox {
                id: inputEncodingComboBox
                // width: parent.width
                selectionModel: decodersModel
                elementToDisplayText: function(e){return e.name.translated}
                elementToRawText: function(e){return e.name.raw}
                property bool byteWiseEncodingSelected: selectionModel.count ?
                    selectionModel.get(currentIndex).bytewise : false
                label: qsTranslate("first-page","Input Encoding")
                description: qsTranslate("first-page",
                    "How to interpret the input")
            }

            TextArea {
                id: inputField
                label: qsTranslate("first-page","Input")
                placeholderText: qsTranslate("first-page", "Enter text")
                font.family: inputEncodingComboBox.byteWiseEncodingSelected ?
                    app.monoFontFamily : Theme.fontFamily
                font.pixelSize: Theme.fontSizeTiny +
                    (Theme.fontSizeMedium - Theme.fontSizeTiny)
                    / ( 1 + (text.length *
                    (1 + 10 * inputEncodingComboBox.byteWiseEncodingSelected) )
                    / 1000 )
                width: parent.width
            }

            SectionHeader {
                text: qsTranslate("first-page", "Examine Input")
                visible: examineInput
            }

            ModelComboBox {
                id: inputBytesEncodingComboBox
                // width: parent.width
                selectionModel: encodersModel
                elementToDisplayText: function(e){return e.name.translated}
                elementToRawText: function(e){return e.name.raw}
                enabled: inputBytesField.enabled
                visible: inputBytesField.visible
                property bool byteWiseEncodingSelected: selectionModel.count ?
                    selectionModel.get(currentIndex).bytewise : false
                label: qsTranslate("first-page","Encoding")
                description: qsTranslate("first-page",
                    "How to display the decoded input")
            }

            TextSwitch {
                id: inputBytesFieldSeparateBytesSwitch
                visible: inputBytesEncodingComboBox.visible
                    && inputBytesEncodingComboBox.byteWiseEncodingSelected
                enabled: inputBytesEncodingComboBox.byteWiseEncodingSelected
                    && inputBytesField.enabled
                text: qsTranslate("first-page","Separate bytes")
                description: qsTranslate("first-page","with whitespace")
            }

            EncodingTransformTextArea {
                id: inputBytesField
                sourceText: inputField.text
                sourceEncoding: inputEncodingComboBox.rawValue
                displayEncoding: inputBytesEncodingComboBox.rawValue
                displaySeparateBytes:
                    inputBytesFieldSeparateBytesSwitch.checked
                    && inputBytesEncodingComboBox.byteWiseEncodingSelected
                visible: examineInput
                width: parent.width
                label: qsTranslate("first-page",
                    "%1-representation of decoded %2 input")
                    .arg(inputBytesEncodingComboBox.value)
                    .arg(inputEncodingComboBox.value)
                placeholderText: qsTranslate("first-page", "Input")
                readOnly: true
                onClicked: {
                    Clipboard.text = text
                    notificationBar.success(qsTranslate("first-page",
                        "%1 copied to clipboard").arg(label))
                }
            }

            Button {
                id: examineToggleButton
                visible: inputField.text != ""
                anchors.horizontalCenter: parent.horizontalCenter
                text: examineInput ? qsTranslate("first-page","Hide")
                    : qsTranslate("first-page","Examine")
                onClicked: { examineInput = ! examineInput }
            }

            SectionHeader {
                text: qsTranslate("first-page", "Hash")
                visible: hashAlgorithmComboBox.visible
            }

            ListModel {
                id: hashAlgoModel
                Component.onCompleted: {
                    python.hashAlgorithmsChanged.connect(function(){
                        console.debug(
                            "Updating hash algorithm selections with %1"
                            .arg(python.hashAlgorithms))
                        clear()
                        python.hashAlgorithms.forEach(function(algo){
                            append({name: algo})
                            })
                    })
                }
            }

            ModelComboBox {
                id: hashAlgorithmComboBox
                visible: inputField.text != ""
                enabled: digestField.enabled
                selectionModel: hashAlgoModel
                elementToDisplayText: function(e){return e.name}
                elementToRawText:     function(e){return e.name}
                label: qsTranslate("first-page","Algorithm")
                description: qsTranslate("first-page",
                    "Hash algorithm to use")
            }

            Slider {
                id: hashIterationsSlider
                minimumValue: 0
                maximumValue: 1000
                width: parent.width
                property int slowValue: 0
                value: 0
                valueText: value
                stepSize: 100
                property bool potentiallyVisible: true
                visible: inputField.text != "" && hashAlgorithmComboBox.visible
                    && potentiallyVisible
                label: qsTranslate("first-page", "Iterations")
                     + (value > 0 ? " - %1".arg(qsTranslate("first-page",
                        "long press to enter manually")) : "")
                onPressAndHold: {
                    hashIterationsField.text = value
                    hashIterationsField.slowValue = value
                    hashIterationsField.potentiallyVisible = true
                    potentiallyVisible = false
                }
                onDownChanged: { if(!down) { slowValue = value }
                }
            }

            TextField {
                id: hashIterationsField
                property bool potentiallyVisible: false
                visible: ! hashIterationsSlider.visible && inputField.text!=""
                    && hashAlgorithmComboBox.visible && potentiallyVisible
                width: parent.width
                label: qsTranslate("frist-page","Hash iterations")
                placeholderText: qsTranslate("first-page",
                    "Enter a positive number of iterations")
                validator: IntValidator { bottom: 0 }
                property int slowValue: 0
                property int value: Number(text)
                onPressAndHold: {
                    if(hashIterationsSlider.maximumValue < value) {
                        hashIterationsSlider.maximumValue =
                            Math.round(value / 100) * 100 * 2
                    }
                    hashIterationsSlider.value = value
                    hashIterationsSlider.slowValue = value
                    hashIterationsSlider.potentiallyVisible = true
                    potentiallyVisible = false
                }
                onFocusChanged: { if(!focus) { slowValue = value }
                }
            }

            ListModel {
                id: encodersModel
                Component.onCompleted: {
                    python.encodersChanged.connect(function(){
                        console.debug(
                            "Updating output encoding selections with %1"
                            .arg(python.encoders))
                        clear()
                        python.encoders.forEach(function(encoder){
                            append(encoder)
                        })
                    })
                }
            }

            ModelComboBox {
                id: hashEncodingComboBox
                label: qsTranslate("first-page","Encoding")
                selectionModel: encodersModel
                elementToDisplayText: function(e){return e.name.translated}
                elementToRawText: function(e){return e.name.raw}
                description: qsTranslate("first-page",
                    "How to encode the hash")
                enabled: digestField.enabled
                visible: digestField.visible
                property bool byteWiseEncodingSelected: selectionModel.count ?
                    selectionModel.get(currentIndex).bytewise : false
            }

            TextSwitch {
                id: digestFieldSeparateBytesSwitch
                visible: hashEncodingComboBox.visible
                    && hashEncodingComboBox.byteWiseEncodingSelected
                enabled: hashEncodingComboBox.byteWiseEncodingSelected
                    && digestField.enabled
                text: qsTranslate("first-page","Separate bytes")
                description: qsTranslate("first-page","with whitespace")
            }

            TextSwitch {
                id: encodedIterationsSwitch
                visible: digestField.hashIterations > 0
                    && hashAlgorithmComboBox.visible
                enabled: digestField.enabled
                text: qsTranslate("first-page","Encoded iterations")
                description: qsTranslate("first-page",
                    "Encode the digest before each iteration")
            }

            HashTextArea {
                id: digestField
                width: parent.width
                visible: hashAlgorithmComboBox.visible
                sourceText: inputField.text
                sourceEncoding: inputEncodingComboBox.rawValue
                displayEncoding: hashEncodingComboBox.rawValue
                hashAlgorithm: hashAlgorithmComboBox.rawValue
                hashIterations: hashIterationsField.visible ?
                    hashIterationsField.slowValue
                    : hashIterationsSlider.slowValue
                encodedIterations: encodedIterationsSwitch.checked
                displaySeparateBytes:
                    digestFieldSeparateBytesSwitch.checked &&
                    hashEncodingComboBox.byteWiseEncodingSelected
                label: qsTranslate("first-page",
                    "%1-representation of %2-digest")
                    .arg(hashEncodingComboBox.value)
                    .arg(hashAlgorithmComboBox.value)
                placeholderText: qsTranslate("first-page", "digest")
                readOnly: true
                onClicked: {
                    Clipboard.text = text
                    notificationBar.success(qsTranslate("first-page",
                        "%1 copied to clipboard").arg(label))
                }
            }

            IconButton {
                id: clipboardButton
                enabled: digestField.enabled
                visible: digestField.visible
                anchors {
                    horizontalCenter: parent.horizontalCenter
                }
                icon.source: "image://theme/icon-m-clipboard"
                onClicked: {
                    Clipboard.text = digestField.text
                    notificationBar.success(qsTranslate("first-page",
                        "%1 copied to clipboard")
                        .arg(digestField.label))
                }
            }

        }
    }
}


