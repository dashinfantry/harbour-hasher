import QtQuick 2.0
import Sailfish.Silica 1.0

MouseArea {
    width: parent.width
    anchors {
        top: parent.top
        left: parent.left
        right: parent.right
    }
    height: box.height
    visible: opacity > 0
    property var clickedCallback: undefined
    opacity: 0
    Behavior on opacity { FadeAnimator {} }

    Rectangle {
        id: box
        width: parent.width
        height: Math.max(icon.height, label.height) + Theme.paddingLarge
        color: Theme.rgba(Theme.highlightDimmerColor, 0.8)

        Icon {
            id: icon
            anchors {
                left: parent.left
                verticalCenter: parent.verticalCenter
                leftMargin: Theme.horizontalPageMargin
            }
            source: "image://theme/icon-m-about"
            color: palette.primaryColor
        }

        Label {
            id: label
            anchors {
                left: icon.right
                leftMargin: Theme.paddingLarge
                right: parent.right
                rightMargin: Theme.horizontalPageMargin
                verticalCenter: parent.verticalCenter
            }
            font.pixelSize: Theme.fontSizeSmall
            wrapMode: Text.WordWrap
            truncationMode: TruncationMode.Fade
            maximumLineCount: 5
        }

    }

    Timer {
        id: timer
        repeat: false
        onTriggered: reset()
    }

    function show (text, iconpath, callback) {
        console.debug("showing notification bar with text %1".arg(text))
        if (callback) {
            clickedCallback = callback
        }
        label.text = text
        icon.source = iconpath || "image://theme/icon-m-about"
        opacity = 1
        timer.interval = 3000
        timer.start()
    }

    function success(text, callback) {
        show(text, "image://theme/icon-m-accept", callback)
    }

    function info(text, callback) {
        show(text, "image://theme/icon-m-about", callback)
    }

    function warning(text, callback) {
        show(text, "image://theme/icon-m-dismiss", callback)
    }

    function error(text, callback) {
        show(text, "image://theme/icon-m-dismiss", callback)
    }

    function reset () {
        hide()
        timer.stop()
        clickedCallback = undefined
    }

    function hide () {
        opacity = 0
    }

    onClicked: {
        if(clickedCallback) {
            hide()
            clickedCallback()
        }
        reset()
    }

}
