import QtQuick 2.0
import Sailfish.Silica 1.0

TextArea {
    property var sourceText: ""
    property var sourceEncoding: undefined
    property var displayEncoding: undefined
    property bool displaySeparateBytes: false
    property bool errorrrr: false
    property var busy: false

    font.family: displaySeparateBytes ? app.monoFontFamily : Theme.fontFamily
    font.pixelSize: Theme.fontSizeTiny +
        (Theme.fontSizeMedium - Theme.fontSizeTiny)
        / ( 1 + (text.length * (1 + 10 * displaySeparateBytes) ) / 1000 )
    enabled: !errorrrr && !busy
    wrapMode: displaySeparateBytes ? TextEdit.WordWrap : TextEdit.WrapAnywhere

    BusyIndicator {
        visible: running
        running: busy
        anchors.centerIn: parent
        size: BusyIndicatorSize.Medium
    }

    function clear () {
        text = ""
    }

    function update () {
        if(visible) {
            console.debug(
                "Telling Python to transform " +
                "'%1' from '%2' to '%3' %4 separation"
                .arg(sourceText)
                .arg(sourceEncoding)
                .arg(displayEncoding)
                .arg(displaySeparateBytes ? "with" : "without")
            )
            busy = true
            python.call("python.app.transform",
                [sourceText, sourceEncoding,
                    displayEncoding, displaySeparateBytes],
                function(transformed){
                if(transformed === undefined) {
                    errorrrr = true
                    text = ""
                } else {
                    errorrrr = false
                    text = transformed
                }
                busy = false
            })
        }
    }

    onVisibleChanged:              update()
    onSourceTextChanged:           update()
    onSourceEncodingChanged:       update()
    onDisplayEncodingChanged:      update()
    onDisplaySeparateBytesChanged: update()

    onClicked: {
        if(!readOnly){
            Clipboard.text = text
        }
    }
}
