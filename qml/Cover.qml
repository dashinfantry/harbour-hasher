import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

CoverBackground {
    id : cover

    Image {
        fillMode: Image.PreserveAspectFit
        anchors.centerIn: parent
        anchors.fill: parent
        clip: true
        source: "../images/cover-background.svg"
        opacity: 0.3
    }

}

