import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

EncodingTransformTextArea {
    property var hashAlgorithm: undefined
    property var hashIterations: 0
    property var encodedIterations: false

    function update () {
        if(visible) {
            console.debug(
                ("Telling Python to calculate %1-encoded " +
                "%2 digest (%3 separation and %4 %5 iterations) " +
                "of '%6' decoded from %7")
                .arg(displayEncoding)
                .arg(hashAlgorithm)
                .arg(displaySeparateBytes ? "with" : "without")
                .arg(hashIterations)
                .arg(encodedIterations ? "encoded" : "raw")
                .arg(sourceText)
                .arg(sourceEncoding)
                )
            busy = true
            python.call("python.app.hash",
                [sourceText, sourceEncoding, hashAlgorithm,
                    displayEncoding, hashIterations,
                    encodedIterations, displaySeparateBytes],
                function(hash){
                if(hash === undefined) {
                    errorrrr = true
                    text = ""
                } else {
                    errorrrr = false
                    text = hash
                }
                busy = false
            })
        }
    }

    onHashAlgorithmChanged:      update()
    onHashIterationsChanged:     update()
    onEncodedIterationsChanged:  update()
}
