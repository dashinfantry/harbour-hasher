import QtQuick 2.0
import Sailfish.Silica 1.0
import io.thp.pyotherside 1.4
import "."

ApplicationWindow
{
    id: app
    property bool loading: false
    property bool portrait: app.orientation == Orientation.PortraitMask
    initialPage: Component { FirstPage { } }
    cover: Qt.resolvedUrl("Cover.qml")
    property var version: undefined
    property var monoFontFamily: Theme.fontFamily

    NotificationBar {
        id: notificationBar
    }

    Python {
        id: python
        onError: {
            notificationBar.warning(
                qsTranslate("app","Python error:\n%1").arg(traceback),
                function () { app.pageStack.push("ErrorPage.qml",
                    {"message": traceback})
                }
            )
        }
        Component.onCompleted: {
            setHandler("app-version",function(v){version = v})
            setHandler("error",function(text){notificationBar.error(text)})
            setHandler("warning",function(text){notificationBar.warning(text)})
            setHandler("info",function(text){notificationBar.info(text)})
            setHandler("success",function(text){notificationBar.success(text)})
            // find first monospace font
            Qt.fontFamilies().some(function(family){
                if(family.toLowerCase().indexOf("mono") != -1) {
                    monoFontFamily = family
                    return true
                }
            })
        }
    }

}


