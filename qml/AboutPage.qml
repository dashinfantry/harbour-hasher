import QtQuick 2.0
import Sailfish.Silica 1.0
import "."

Page {

    id: aboutpage

    SilicaFlickable {
        id: view
        anchors.fill: parent
        contentHeight: column.height

        VerticalScrollDecorator {}

        Column {
            id: column
            spacing: Theme.paddingLarge
            width: parent.width

            PageHeader {
                id: header
                width: parent.width
                title: qsTranslate("about-page","About Hasher")
            }

            Image {
                id: appIcon
                anchors.horizontalCenter: parent.horizontalCenter
                asynchronous: true
                fillMode: Image.PreserveAspectFit
                source: "../images/harbour-hasher.svg"
                sourceSize {
                    width: parent.width / 2
                    height: parent.width / 2
                }
            }

            Label {
                id: appNameLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page", "Hasher")
                font.bold: true
                font.pixelSize: Theme.fontSizeLarge
            }

            Label {
                id: appVersionLabel
                anchors.horizontalCenter: parent.horizontalCenter
                text: app.version
                font.bold: true
            }

            Label {
                id: appDescriptionLabel
                width: parent.width
                anchors {
                    rightMargin: Theme.horizonalPageMargin
                    leftMargin: Theme.horizonalPageMargin
                    horizontalCenter: parent.horizontalCenter
                }
                horizontalAlignment: Text.AlignHCenter
                text: qsTranslate("about-page",
                    "SailfishOS app to calculate digests")
                font.pixelSize: Theme.fontSizeSmall
                wrapMode: Text.WordWrap
            }

            SectionHeader {
                text: qsTranslate("about-page", "Translations")
            }

            ListModel {
                id: translationModel
                Component.onCompleted: {
                    append({
                        "language":qsTranslate("language","English"),
                        "translator":"Yann Büchau",
                        "link": "https://gitlab.com/nobodyinperson"
                    })
                    append({
                        "language":qsTranslate("language","German"),
                        "translator":"Yann Büchau",
                        "link": "https://gitlab.com/nobodyinperson"
                    })
                    append({
                        "language":qsTranslate("language","French"),
                        "translator":"Yann Büchau",
                        "link": "https://gitlab.com/nobodyinperson"
                    })
                    append({
                        "language":qsTranslate("language","Swedish"),
                        "translator":"Åke Engelbrektson",
                        "link": "https://gitlab.com/eson"
                    })
                }
            }

            Repeater {
                model: translationModel
                delegate: Label {
                    id: label
                    textFormat: Text.RichText
                    horizontalAlignment: Text.AlignHCenter
                    font.pixelSize: Theme.fontSizeMedium
                    text : ("%1 - <style>a:link{color: %2;}</style>" +
                        "<a href=\"%3\">%4</a>")
                        .arg(model.language)
                        .arg(Theme.highlightColor)
                        .arg(model.link)
                        .arg(model.translator)
                    truncationMode: TruncationMode.Fade
                    width: parent.width
                    onLinkActivated: Qt.openUrlExternally(link)
                }
            }

            SectionHeader {
                text: qsTranslate("about-page", "Links")
            }

            Button {
                id: sourceCodeButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page","Source Code")
                onClicked: {
                    Qt.openUrlExternally(
                        "https://gitlab.com/nobodyinperson/harbour-hasher"
                        )
                }
            }

            Button {
                id: donateButton
                anchors.horizontalCenter: parent.horizontalCenter
                text: qsTranslate("about-page","Donate")
                onClicked: Qt.openUrlExternally("https://tinyurl.com/yycbh8so")
            }

            Rectangle {
                width: parent.width
                height: Theme.paddingLarge
                opacity: 0
            }

        }
    }
}



